import React, { useState, createRef, useEffect } from "react";
import {
  TouchableOpacity,
  Image,
  View,
  FlatList,
  StyleSheet,
  Dimensions
} from "react-native";

const windowSize = Dimensions.get("screen");

const sliderItemStyle = StyleSheet.create({
  item: {
    height: 280,
    width: windowSize.width
  }
});

const sliderIndicatorStyle = StyleSheet.create({
  indicatorContainer: {
    position: "absolute",
    bottom: 16,
    flexDirection: "row",
    justifyContent: "center",
    width: windowSize.width
  }
});

const SliderItem = function(props) {
  return (
    <TouchableOpacity>
      <Image
        style={sliderItemStyle.item}
        source={{
          uri: props.url
        }}
      />
    </TouchableOpacity>
  );
};

const SliderIndicator = function(props) {
  return props.active ? (
    <Image source={require("../assets/slideOn.png")} />
  ) : (
    <Image source={require("../assets/slideOff.png")} />
  );
};

const Slider = function(props) {
  const [index, setIndex] = useState(0);
  // const sliderRef = createRef();
  // useEffect(function() {
  //   setInterval(() => {
  //       sliderRef.current.scrollToIndex({
  //         index: index === props.data.length - 1 ? 0 : index + 1
  //       });
  //   }, 1000);
  // }, []);
  return (
    <View>
      <FlatList
        // ref={sliderRef}
        horizontal
        data={props.data}
        renderItem={function(data) {
          return <SliderItem url={data.item} />;
        }}
        snapToAlignment={"start"}
        snapToInterval={windowSize.width + 10}
        decelerationRate={"fast"}
        onScroll={function(e) {
          let offset = e.nativeEvent.contentOffset.x;
          let index = parseInt(offset / windowSize.width); // your cell height
          setIndex(index);
        }}
      />
      {/* Indicator */}
      <View style={sliderIndicatorStyle.indicatorContainer}>
        {props.data.map(function(_, i) {
          return <SliderIndicator key={i} active={index === i} />;
        })}
      </View>
    </View>
  );
};

export default Slider;
