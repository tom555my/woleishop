import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Image
} from 'react-native';

const styles = StyleSheet.create({
  navBarContainer: {
    alignItems: 'center',
    padding: 8
  },
  navBarText: {
    fontSize: 17,
    lineHeight: 22
  },
  confirmBtnText: {
    fontSize: 16,
    color: 'rgb(151, 151, 151)'
  },
  leftBtnContainer: {
    position: 'absolute',
    top: 8,
    left: 8,
    bottom: 8
  },
  rightBtnContainer: {
    position: 'absolute',
    top: 8,
    right: 8,
    bottom: 8
  }
})

const NavBar = function (props) {
  return (
    <View style={styles.navBarContainer}>
      <TouchableOpacity style={styles.leftBtnContainer}>
        <Image source={require('../assets/backLight.png')}/>
      </TouchableOpacity>
      <Text style={styles.navBarText}>
        {props.title}
      </Text>
      <TouchableOpacity style={styles.rightBtnContainer}>
        <Text style={styles.confirmBtnText}>確認</Text>
      </TouchableOpacity>
    </View>
  )
}

export default NavBar;