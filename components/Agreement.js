import React, { useState } from "react";
import { TouchableOpacity, Text, Image, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  agreementContainer: {
    flexDirection: "row",
    alignItems: "center"
  },
  agreementText: {
    fontSize: 16,
    marginLeft: 8
  }
});

/**
 * onChange(value: boolean): void
 */
const Agreement = function(props) {
  const [state, setState] = useState(true);
  return (
    <TouchableOpacity
      style={styles.agreementContainer}
      onPress={function() {
        setState(!state);
        props.onChange && props.onChange(!state);
      }}
    >
      {state ? (
        <Image source={require("../assets/checkboxOn.png")} />
      ) : (
        <Image source={require("../assets/checkboxOff.png")} />
      )}

      <Text style={styles.agreementText}>收件人與購買人資料相同</Text>
    </TouchableOpacity>
  );
};

export default Agreement;
