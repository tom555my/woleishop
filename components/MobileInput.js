import React from "react";
import { View, Text, TextInput, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  inputContainer: {
    borderWidth: 1.5,
    borderColor: "rgb(150, 150, 150)",
    borderRadius: 6,
    flexDirection: "row"
  },
  countryCodeContainer: {
    height: 54,
    width: 73,
    alignItems: "center",
    justifyContent: "center",
    borderRightWidth: 1.5,
    borderRightColor: "rgb(150, 150, 150)"
  },
  countryCodeText: {
    fontSize: 16,
    lineHeight: 25,
    color: "rgb(151, 151, 151)"
  },
  textInput: {
    fontSize: 16,
    padding: 16
  }
});

const MobileInput = function() {
  return (
    <View style={styles.inputContainer}>
      <View style={styles.countryCodeContainer}>
        <Text style={styles.countryCodeText}>+852</Text>
      </View>
      <TextInput style={styles.textInput} />
    </View>
  );
};

export default MobileInput;
