import React from "react";
import {
  TextInput,
  StyleSheet
} from "react-native";

const inputStyles = StyleSheet.create({
  input: {
    borderWidth: 1.5,
    borderColor: 'rgb(237, 237, 237)',
    borderRadius: 6,
    padding: 16,
    fontSize: 16,
    lineHeight: 25
  }
});

/**
 * placeholder <string> Placeholder Text
 */
const Input = function(props) {
  return (
    <TextInput
      style={inputStyles.input}
      placeholder={props.placeholder}
    />
  );
};

export default Input;