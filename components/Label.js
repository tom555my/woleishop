import React from "react";
import { Text, StyleSheet } from "react-native";

const styles = StyleSheet.create({
  text: {
    fontSize: 14,
    lineHeight: 25
  }
});

/**
 * text <string> Text of the label
 */
const Label = function(props) {
  return <Text style={styles.text}>{props.text}</Text>;
};

export default Label;
