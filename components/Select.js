import React from "react";
import { StyleSheet, View, Image, Text } from "react-native";
import RNPickerSelect from "react-native-picker-select";

const styles = StyleSheet.create({
  selectContainer: {
    borderWidth: 1.5,
    borderColor: "rgb(237, 237, 237)",
    borderRadius: 6,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 16
  },
  selectText: {
    fontSize: 16,
    lineHeight: 25
  },
  placeholderText: {
    fontSize: 16,
    lineHeight: 25,
    color: "rgb(151,151,151)"
  }
});

/**
 * style <object> Style of Select
 * placeholder <string> Placeholder value
 * value <string> Value string
 * items <[{label: string, value: string}]> Options
 */
const Select = function(props) {
  return (
    <View style={[styles.selectContainer, props.style]}>
      <RNPickerSelect
        onValueChange={function(value, index) {}}
        placeholder={{
          label: props.placeholder
        }}
        placeholderTextColor={"rgb(151,151,151)"}
        items={props.items}
      />
      <Image source={require("../assets/arrowSmallRightGrey.png")} />
    </View>
  );
};

export default Select;
