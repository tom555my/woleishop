import React, { useState } from "react";
import {
  Modal,
  TouchableOpacity,
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  Picker
} from "react-native";

const windowSize = Dimensions.get("window");

const styles = StyleSheet.create({
  btn: {
    borderWidth: 1.5,
    borderColor: "rgb(237,237,237)",
    borderRadius: 6,
    padding: 16,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  text: {
    color: "rgb(151, 151, 151)",
    fontSize: 16
  },
  arrowIcon: {
    width: 24,
    height: 24
  },
  modalContainer: {
    width: windowSize.width,
    height: windowSize.height,
    alignItems: "stretch",
    opacity: 0.4,
    backgroundColor: "#000000"
  },
  modalSelector: {
    borderRadius: 10,
    backgroundColor: "#ffffff",
    height: 375,
    position: "absolute",
    width: windowSize.width,
    bottom: 0
    // paddingVertical: 11,
    // paddingHorizontal: 16
  },
  modalBackdrop: {
    width: windowSize.width,
    height: windowSize.height,
    alignItems: "stretch",
    opacity: 0.4,
    backgroundColor: "#000000"
  },
  modalHeader: {
    paddingHorizontal: 16,
    paddingVertical: 11,
    alignItems: "center",
    height: 42,
    borderBottomWidth: 3,
    flexDirection: "row",
    backgroundColor: "#ff0000"
  },
  modalHeaderConfirm: {},
  modalHeaderText: {
    fontSize: 16,
    color: "#000000"
  }
});

/**
 * onChange(item)
 * placeholder <string>
 * items <{label: string, value: string}[]>
 * style <object>
 */
const ModalPicker = function(props) {
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [selectedItem, setSelectedItem] = useState();
  return (
    <>
      <TouchableOpacity
        style={[styles.btn, props.style]}
        onPress={function() {
          setIsOpenModal(true);
        }}
      >
        <Text style={styles.text}>
          {selectedItem == null
            ? props.value
              ? props.items.find(item => item.value == props.value).label
              : props.placeholder
            : selectedItem.label}
        </Text>
        <Image
          style={styles.arrowIcon}
          source={require("../assets/arrowSmallRightGrey.png")}
        />
      </TouchableOpacity>
      <Modal animationType={"slide"} visible={isOpenModal}>
        <TouchableOpacity
          style={styles.modalBackdrop}
          onPress={function() {
            setIsOpenModal(false);
          }}
          activeOpacity={1}
        />
        <View style={styles.modalSelector}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              paddingBottom: 11,
              borderBottomWidth: 1,
              borderBottomColor: "rgb(237, 237, 237)",
              paddingHorizontal: 16,
              paddingVertical: 11,
              height: 42
            }}
          >
            <View style={{ flex: 1, width: 100, height: 100 }}>
              <Text style={{ fontSize: 16, textAlign: "center" }}>
                {props.placeholder}
              </Text>
            </View>
            <TouchableOpacity
              style={{ position: "absolute", right: 16, top: 11, bottom: 11 }}
              onPress={function() {
                props.onChange && props.onChange(selectedItem);
                setIsOpenModal(false);
              }}
            >
              <Text style={{ fontSize: 16 }}>確認</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{ flex: 1, alignItems: "stretch", justifyContent: "center" }}
          >
            <Picker
              selectedValue={
                selectedItem == null ? props.value : selectedItem.value
              }
              onValueChange={(val, pos) => {
                setSelectedItem(props.items[pos]);
              }}
            >
              {props.items.map((item, i) => (
                <Picker.Item key={i} label={item.label} value={item.value} />
              ))}
            </Picker>
          </View>
        </View>
      </Modal>
    </>
  );
};

export default ModalPicker;
