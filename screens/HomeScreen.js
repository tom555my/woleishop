import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import SettingsScreen from "./SettingsScreen";
import ProductScreen from "./ProductScreen";
import StudyScreen from "./StudyScreen";
import Slider from '../components/Slider'

const Tab = createBottomTabNavigator();

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "stretch"
  },
  button: {
    padding: 16,
    borderWidth: 1,
    borderRadius: 6
  },
  tabBarLabelFont: {
    fontSize: 10,
    color: "rgb(151, 151,151)"
  },
  tabBarLabelFocusedFont: {
    fontSize: 10,
    color: "#000000"
  }
});

const HomeScreen = function() {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <Slider data={[
        'https://dummyimage.com/600x400/fff/000',
        'https://dummyimage.com/600x400/fff/000',
        'https://dummyimage.com/600x400/fff/000'
      ]}/>
    </View>
  );
};

const HomeTabNavigator = function() {
  return (
    <Tab.Navigator
      screenOptions={function(router) {
        return {
          tabBarIcon: function(tab) {
            switch (router.route.name) {
              case "Home":
                if (tab.focused) {
                  return <Image source={require("../assets/homeOn.png")} />;
                } else {
                  return <Image source={require("../assets/homeOff.png")} />;
                }

              case "Product":
                if (tab.focused) {
                  return <Image source={require("../assets/buyOn.png")} />;
                } else {
                  return <Image source={require("../assets/buyOff.png")} />;
                }

              case "Study":
                if (tab.focused) {
                  return <Image source={require("../assets/learnOn.png")} />;
                } else {
                  return <Image source={require("../assets/learnOff.png")} />;
                }

              case "Settings":
                if (tab.focused) {
                  return <Image source={require("../assets/settingOn.png")} />;
                } else {
                  return <Image source={require("../assets/settingOff.png")} />;
                }

              default:
                return null;
            }
          },
          tabBarLabel: function(tab) {
            switch (router.route.name) {
              case "Home":
                return (
                  <Text
                    style={[
                      styles.tabBarLabelFont,
                      tab.focused && styles.tabBarLabelFocusedFont
                    ]}
                  >
                    主頁
                  </Text>
                );

              case "Product":
                return (
                  <Text
                    style={
                      tab.focused
                        ? styles.tabBarLabelFocusedFont
                        : styles.tabBarLabelFont
                    }
                  >
                    買嘢
                  </Text>
                );

              case "Study":
                return (
                  <Text
                    style={[
                      styles.tabBarLabelFont,
                      tab.focused && styles.tabBarLabelFocusedFont
                    ]}
                  >
                    學嘢
                  </Text>
                );

              case "Settings":
                return (
                  <Text
                    style={[
                      styles.tabBarLabelFont,
                      tab.focused && styles.tabBarLabelFocusedFont
                    ]}
                  >
                    設定
                  </Text>
                );

              default:
                return null;
            }
          }
        };
      }}
    >
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="Product" component={ProductScreen} />
      <Tab.Screen name="Study" component={StudyScreen} />
      <Tab.Screen name="Settings" component={SettingsScreen} />
    </Tab.Navigator>
  );
};

export default HomeTabNavigator;
