import React, { useState } from "react";
import { View, Dimensions, StyleSheet } from "react-native";
import NavBar from "../components/NavBar";
import Label from "../components/Label";
import Input from "../components/Input";
import Agreement from "../components/Agreement";
import ModalPicker from "../components/ModalPicker";
import MobileInput from "../components/MobileInput";

var windowSize = Dimensions.get("screen");

const styles = StyleSheet.create({
  container: {
    height: windowSize.height,
    width: windowSize.width,
    paddingTop: 20
  },
  mainSession: {
    padding: 16
  },
  regionContainer: {
    flexDirection: "row"
  }
});

const _renderInputs = function() {
  return (
    <>
      <Label text={"收件人姓名"} />
      <Input placeholder={"收件人姓名"} />
      <View style={{ height: 16 }} />
      <Label text={"收件人聯絡電話"} />
      <MobileInput />
    </>
  );
};

const MainScreen = function() {
  const [isAddressSame, setIsAddressSame] = useState(true);
  return (
    <View style={styles.container}>
      <NavBar title={"送貨資料"} />
      <View style={styles.mainSession}>
        <Label text={"送貨方式"} />
        <ModalPicker
          placeholder={"送貨方式"}
          value={"ship"}
          items={[
            {
              label: "自取（HK$0）",
              value: "self"
            },
            {
              label: "寄貨（HK$30）",
              value: "ship"
            }
          ]}
        />
        <View style={{ height: 16 }} />
        <Label text={"地區"} />
        <View style={styles.regionContainer}>
          <ModalPicker
            placeholder={"請選擇"}
            style={{ width: 150 }}
            items={[
              {
                label: "九龍",
                value: "KL"
              },
              {
                label: "新界",
                value: "NT"
              },
              {
                label: "港島",
                value: "HKI"
              }
            ]}
          />
          <ModalPicker
            placeholder={"請選擇"}
            style={{
              flex: 1,
              marginLeft: 8,
              backgroundColor: "rgb(237, 237, 237)"
            }}
            items={[
              {
                label: "九龍",
                value: "KL"
              },
              {
                label: "新界",
                value: "NT"
              },
              {
                label: "港島",
                value: "HKI"
              }
            ]}
          />
        </View>
        <View style={{ height: 16 }} />
        <Label text={"地址"} />
        <Input placeholder={"包含街道名稱、門牌號碼、樓層"} />
        <View style={{ height: 24 }} />
        <Agreement
          onChange={function(val) {
            console.log(val);
            setIsAddressSame(val);
          }}
        />
        <View style={{ height: 24 }} />
        {!isAddressSame && _renderInputs()}
      </View>
    </View>
  );
};

export default MainScreen;
