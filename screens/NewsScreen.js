import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  button: {
    padding: 16,
    borderWidth: 1,
    borderRadius: 6
  }
});

const NewsScreen = function() {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      {/* Button */}
      <TouchableOpacity style={styles.button} onPress={function(){
        navigation.goBack()
      }}>
        <Text>Welcome to News Screen</Text>
      </TouchableOpacity>
    </View>
  );
};

export default NewsScreen;
